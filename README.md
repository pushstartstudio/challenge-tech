# Desafio PushStart

Crie um código que recebe uma matriz bidimensional e uma lista de peças com seus respectivos tamanhos e números de identificação.

O código deve encaixar de forma aleatória todas as peças na matriz, sem que haja sobreposição entre elas. A cada execução, o resultado deve ser diferente do anterior.

O código deve suportar o uso de diferentes tamanhos de matriz, quantidades de peças e formatos das peças.

O resultado do processamento deve ser uma matriz preenchida com os identificadores dos blocos.

![Ilustração de blocos](challenge.png)

Obs: não é necessário exibir o visual como na imagem a cima, basta que o código retorne a matriz conforme mostrado no exemplo de saída do algorítmo.

## Regras

* O desafio pode ser resolvido em qualquer linguagem de programação
* O código deve estar disponível no Bitbucket, GitHub ou GitLab
* O repositório deve incluir instruções para execução do código
* A definição dos formatos de entrada e saída é livre
* A organização do código é um critério para avaliação

## Desafio extra

Depois de preencher a matriz com as peças, o código deve gerar três afirmações aleatórias sobre a matriz preenchida.

### Exemplo

Considere a matriz gerada pelo desafio inicial:

    [4,4,4,4,4,4,4,4]
    [2,2,1,1,1,1,1,1]
    [2,2,1,1,1,1,1,1]
    [3,6,5,5,6,5,5,3]
    [3,6,6,5,5,6,6,3]

É possível gerar as afirmações aleatórias:

    "A peça de ID 4 está acima da peça de ID 2"
    "Uma peça de ID 5 está à direita de uma peça de ID 6"
    "Uma peça de ID 3 está abaixo da peça de ID 1"
    